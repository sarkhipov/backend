<?php

namespace common\models;

use common\components\Helper;
use common\components\location\Finder;
use common\components\meta\OfferFlatMeta;
use common\components\meta\OfferMeta;
use common\models\base\OfferRelation;
use common\models\dict\Locality;
use common\models\dict\Metro;
use common\models\dict\SubLocality;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use Yii;


/**
 * This is the model class for table "offer".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $category
 * @property integer $location_id
 * @property integer $user_id
 * @property string $sales_name
 * @property string $sales_phone
 * @property string $sales_type
 * @property string $sales_organization
 * @property integer $price
 * @property string $date_created
 * @property string $date_updated
 * @property string $date_expired
 * @property string $description
 * @property string $currency
 * @property integer $price_type
 * @property integer $price_period
 * @property boolean mortgage
 * @property boolean haggle
 * @property integer deal_status
 * @property integer contract_status
 * @property integer sell_status
 * @property boolean rent_pledge
 * @property integer agent_fee
 * @property boolean not_for_agents
 * @property integer prepayment
 * @property boolean communal_payments
 * @property boolean with_pets
 * @property boolean with_children
 * @property boolean in_archive
 * @property integer views_total
 * @property integer views_today
 * @property string views_today_date
 * @property string internal_id
 * @property integer $time_on_foot
 * @property integer $time_on_transport
 * @property boolean $is_approved
 * @property boolean $sync
 */
class Offer extends \yii\db\ActiveRecord
{
	private $imagesList;

	public $address;
	public $address_short;
	public $metroTime;
	public $metroTimeType;
	public $locationDetails;
	public $terms;
	public $tempImages;


	/** @var OfferFlat $flat */
	public $flat;

	/**
	 *
	 */
	private function setMetroDistance()
	{
		$this->time_on_foot = $this->isMetroTypeFoot() ? $this->metroTime : null;
		$this->time_on_transport = $this->isMetroTypeTransport() ? $this->metroTime : '';
	}

	/**
	 *
	 */
	private function extractMetroDistance()
	{
		if ($this->time_on_foot) {
			$this->metroTimeType = OfferMeta::METRO_TRANSPORT_ON_FOOT;
			$this->metroTime = $this->time_on_foot;
		}
		if ($this->time_on_transport) {
			$this->metroTimeType = OfferMeta::METRO_TRANSPORT_ON_TRANSPORT;
			$this->metroTime = $this->time_on_transport;
		}
	}

	/**
	 * @param string $scenario
	 * @return $this
	 */
	public function initialize($scenario = '')
	{
		$this->setScenario($scenario);
		$this->type = OfferMeta::TYPE_SALE;
		$this->category = OfferMeta::CATEGORY_FLAT;

		$this->flat = new OfferFlat();
		$this->flat->initialize();
		$this->flat->setScenario($scenario);
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'offer';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['type', 'category', 'price'], 'required'],
			['address', 'required', 'on' => 'create'],
			['address', 'required', 'on' => 'update'],

			['terms', 'required', 'requiredValue' => 1, 'message' => 'Необходимо согасие с условиями', 'on' => 'create'],
			['terms', 'validateOffersCount', 'on' => 'create'],

			[[
				'type', 'category', 'location_id', 'user_id',
				'deal_status', 'contract_status', 'agent_fee',
				'price_type', 'price_period', 'sell_status', 'prepayment', 'metroTime',
				'views_today', 'views_total', 'agency_id',
				'time_on_foot', 'time_on_transport'
			], 'integer', 'min' => 0],
			['price', 'validatePrice'],

			['address', 'validateLocality', 'on' => 'create'],
			['address', 'validateLocality', 'on' => 'update'],

			[[
				'date_created', 'date_updated', 'views_today_date', 'description',
				'currency', 'description', 'tempImages', 'rent_pledge', 'metroTimeType',
				'date_expired', 'internal_id', 'sales_type', 'sales_organization'
			], 'safe'],
			[[
				'mortgage', 'haggle', 'not_for_agents', 'communal_payments',
				'with_pets', 'with_children', 'in_archive', 'is_approved', 'sync'
			], 'boolean'],
			[['sales_name', 'sales_phone'], 'string', 'max' => 128]
		];
	}


	/**
	 *
	 */
	public function afterFind()
	{
		$this->extractMetroDistance();
		parent::afterFind();
	}


	/**
	 * @param bool $insert
	 * @return bool
	 */
	public function beforeSave($insert)
	{
		$this->price = Helper::clearPrice($this->price);
		$this->setMetroDistance();
		return parent::beforeSave($insert);
	}

	/**
	 * @return bool
	 */
	public function loadWithRelation()
	{
		$post = Yii::$app->request->post();
		if (!$this->load($post)) {
			return false;
		}
		/** @var OfferRelation $related */
		$related = $this->{$this->getCategoryAlias()};
		$r = $related->load($post);
		if (!$r) {
			return false;
		}
		$related->parent_attributes = $this->attributes;
		return true;
	}

	/**
	 * @param $field
	 * @return null
	 */
	public function getLabel($field)
	{
		$list = ArrayHelper::merge(
			$this->attributeLabels(),
			$this->getCategoryRelation(true)->attributeLabels()
		);
		return Helper::getFromMap($list, $field);
	}

	/**
	 * @return null
	 */
	public function getCategoryAlias()
	{
		return OfferMeta::getCategoryAlias($this->category);
	}

	/**
	 * @param bool $isNewModel
	 * @return OfferRelation
	 */
	public function getCategoryRelation($isNewModel = false)
	{
		return $this->{$this->getCategoryAlias() . (!$isNewModel ? 'Data' : '')};
	}

	/**
	 * @return bool
	 */
	public function validateWithRelation()
	{
		return $this->validate() && $this->{$this->getCategoryAlias()}->validate();
	}

	/**
	 *
	 */
	public function validateLocality()
	{
		$locationDetails = (!$this->locationDetails)
			? (new Finder())->getLocationDetailsByQuery($this->address)
			: $this->locationDetails;

		$localityName = Helper::getFromMap($locationDetails, 'LocalityName');
		$localityArea = Helper::getFromMap($locationDetails, 'AdministrativeAreaName');

		if (!$locationDetails || (!$localityName && !$localityArea)) {
			$this->addError('address', 'Адрес указан некорректно');
			return;
		}
		if ($localityName !== Locality::DEFAULT_LOCALITY
			&& $localityArea !== Locality::DEFAULT_LOCALITY
			&& $localityArea !== Locality::DEFAULT_LOCALITY_AREA
		) {
			$this->addError('address', 'Размещаемый объект должен находиться в Санкт-Петербурге или Ленинградской области');
		}
	}

	/**
	 *
	 */
	public function validatePrice()
	{
		$value = Helper::clearPrice($this->price);
		if (!is_numeric($value) || $value <= 0) {
			$this->addError('price', 'Значение должно быть положительным числом');
		}
	}

	/**
	 *
	 */
	public function validateOffersCount()
	{
		if (isset(Yii::$app->user) && !Yii::$app->user->identity->canCreateOffer()) {
			$this->addError('terms', 'Вы не можете подать объявление');
		}
	}

	/**
	 * @return string
	 */
	public function getMetroDistance()
	{
		$result = Location::getMetroDistanceTitle($this->time_on_foot, $this->time_on_transport);
		return ($result) ? $result : $this->location->getMetroDistance();
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLocation()
	{
		return $this->hasOne(Location::className(), ['id' => 'location_id']);
	}

	/**
	 * @return OfferImage []
	 */
	public function getImages()
	{
		return $this->hasMany(OfferImage::className(), ['offer_id' => 'id'])->orderBy('id ASC');
	}


	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFlatData()
	{
		return $this->hasOne(OfferFlat::className(), ['offer_id' => 'id']);
	}

	/**
	 * @return OfferFlat
	 */
	public function getFlatModel()
	{
		return $this->getCategoryRelation();
	}

	/**
	 * @param bool $withSize
	 * @param bool $withThumbs
	 * @return array
	 */
	public function getImagesList($withSize = false, $withThumbs = false)
	{
		$result = [];
		foreach ($this->images as $i) {
			$result[] = ($withSize) ? ([
				'file' => $i->url,
				'name' => $i->url,
				'size' => OfferImage::getFileSize($i->url)
			]) : (($withThumbs)
				? ['full' => $i->url, 'thumb' => OfferImage::getThumbName($i->url)]
				: $i->url);
		}
		return $result;
	}

	/**
	 * @return mixed
	 */
	public function getAllAttributes()
	{
		$result = Helper::unsetKeys($this->attributes, [
			'location_id', 'user_id', 'views_today_date'
		]);

		$result = ArrayHelper::merge($result, $this->getCategoryRelation()->getAllAttributes());

		$result['date_created'] = Helper::date($this->date_created, 'd.m.Y');
		$result['date_updated'] = Helper::date($this->date_updated, 'd.m.Y');
		$result['views_today'] = $this->getTodayViews();
		$result['views_total'] = ($this->views_total) ? $this->views_total : 0;

		$result['title'] = $this->getTitle();
		$result['shortTitle'] = $this->getShortTitle();
		$result['titleClear'] = $this->getTitle(true);
		$result['shortDescription'] = $this->getShortDescription();

		$result['type_id'] = $result['type'];
		$result['category_id'] = $result['category'];
		$result['price_type_id'] = $result['price_type'];
		$result['price_per_meter'] = ($this->type == OfferMeta::TYPE_SALE && $this->price_type == OfferMeta::PRICE_TYPE_PER_OFFER && $result['area'])
			? ((int)($this->price / $result['area'])) : '';

		$namedFields = ['type', 'category', 'price_type', 'currency', 'sell_status', 'deal_status', 'contract_status'];
		foreach ($namedFields as $f) {
			$camelF = Inflector::camelize($f);
			$metaMethod = 'get' . $camelF . 'Name';
			if (method_exists(OfferMeta::className(), $metaMethod)) {
				$result[$f] = OfferMeta::$metaMethod($this->$f);
			}
		}

		$result['is_editable'] = $this->isEditable();
		$result['is_approved'] = $this->getIsApproved();
		$result['metro_distance'] = $this->getMetroDistance();

		return $result;
	}

	/**
	 * @param bool|false $clear
	 *
	 * @return string
	 */
	public function getTitle($clear = false)
	{
		$title = $this->getShortTitle($clear);
		if ($this->getCategoryAlias() == 'flat') {
			$title .= ', ' . OfferMeta::FLOOR_TITLE . $this->getFlatModel()->floor . '/' . $this->getFlatModel()->floors_total;
		}
		return $title;
	}

	/**
	 * @return int
	 */
	public function getTodayViews()
	{
		if (Helper::date($this->views_today_date, 'd.m.Y') != date('d.m.Y')) {
			return 0;
		}
		return ($this->views_today) ? $this->views_today : 0;
	}


	/**
	 * @param $locationDetails
	 * @return string
	 */
	public function getShortAddress($locationDetails)
	{
		$result = '';
		if (isset($locationDetails['ThoroughfareName'])) {
			$result .= $locationDetails['ThoroughfareName'];
			$result .= (isset($locationDetails['PremiseNumber']) && $locationDetails['PremiseNumber'])
				? ', ' . $locationDetails['PremiseNumber']
				: '';
			if (isset($locationDetails['LocalityName']) && $locationDetails['LocalityName'] !== Locality::DEFAULT_LOCALITY) {
				$result = $locationDetails['LocalityName'] . ', ' . $result;
			}
		} else {
			$result .= (isset($locationDetails['LocalityName'])) ? $locationDetails['LocalityName'] : '';
			if (isset($locationDetails['SubAdministrativeAreaName'])
				&& $locationDetails['SubAdministrativeAreaName'] !== Locality::DEFAULT_LOCALITY_AREA
				&& $locationDetails['SubAdministrativeAreaName'] !== Locality::DEFAULT_LOCALITY
			) {
				$result = $locationDetails['SubAdministrativeAreaName'] . ($result ? ', ' . $result : '');
			}
		}
		return $result;
	}


	/**
	 * @return bool
	 */
	public function isMetroTypeFoot()
	{
		return $this->metroTimeType == OfferMeta::METRO_TRANSPORT_ON_FOOT;
	}

	/**
	 * @return bool
	 */
	public function isMetroTypeTransport()
	{
		return $this->metroTimeType == OfferMeta::METRO_TRANSPORT_ON_TRANSPORT;
	}

	/**
	 * @return bool
	 */
	public function isEditable()
	{
		return ($this->user_id === Yii::$app->user->id) && !$this->internal_id;
	}

	/**
	 * @return bool
	 */
	public function getIsApproved()
	{
		return ($this->is_approved || $this->internal_id);
	}


	/**
	 * @param null $userId
	 * @return bool
	 */
	public function create($userId = null)
	{
		if (!$userId) {
			$userId = Yii::$app->user->id;
		}
		$location = $this->findLocation();
		$this->location_id = $location->id;
		$this->user_id = $userId;
		if (!$this->save()) {
			return false;
		}
		$this->saveRelation();
		$this->saveImages();
		return true;
	}


	/**
	 * @param $data
	 * @return bool
	 */
	public static function createByData($data)
	{
		$offer = new Offer();
		$offer->initialize('create');
		$offer->attributes = $data;
		$offer->terms = 1;
		$offer->metroTime = $data['metroTime'];
		$offer->metroTimeType = $data['metroTimeType'];
		$offer->locationDetails = $data['locationDetails'];

		if (!$offer->validate()) {
			return false;
		}

		$location = $offer->findLocation();
		$offer->location_id = $location->id;
		if (!$offer->save()) {
			return false;
		}

		//
		/** @var OfferRelation $related */
		$related = $offer->{$offer->getCategoryAlias()};
		$related->attributes = $data[$offer->getCategoryAlias()];
		$related->offer_id = $offer->id;
		$related->scenario = 'default';
		if (!$related->save()) {
			return false;
		}

		//
		OfferImage::updateRemoteByOffer($offer->id, $data['images']);
		return true;
	}


	/**
	 * @return $this|Location|null
	 */
	public function findLocation()
	{
		$locationDetails = (!$this->locationDetails)
			? (new Finder())->getLocationDetailsByQuery($this->address)
			: $this->locationDetails;

		$this->address_short = $this->getShortAddress($locationDetails);
		$locality = Locality::getOne(['name' => Locality::DEFAULT_LOCALITY]);

		//
		$subLocality = null;
		if ($locationDetails['district']) {
			$subLocality = SubLocality::getOne([
				'name' => $locationDetails['district']['Name'], 'locality_id' => $locality->id
			]);
		}

		//
		$metro = null;
		$timeOnFoot = null;
		$timeOnTransport = null;
		if ($locationDetails['metro']) {
			$metro = Metro::getOne([
				'name' => Metro::clearName($locationDetails['metro']['Name']),
				'line' => $locationDetails['metro']['Line'],
				'locality_id' => $locality->id
			]);
			$timeOnFoot = $this->isMetroTypeFoot() ? $this->metroTime : '';
			$timeOnTransport = $this->isMetroTypeTransport() ? $this->metroTime : '';
		}

		//
		$location = Location::getOne([
			'address' => $this->address_short,
			'locality_id' => $locality->id,
			'metro_id' => ($metro) ? $metro->id : null,
			'sub_locality_id' => ($subLocality) ? $subLocality->id : '',
			'latitude' => $locationDetails['Latitude'],
			'longitude' => $locationDetails['Longitude'],
			'time_on_foot' => $timeOnFoot,
			'time_on_transport' => $timeOnTransport,
			'full_address' => $this->address
		]);
		return $location;
	}

	/**
	 * @return mixed
	 */
	public function saveRelation()
	{
		$related = $this->{$this->getCategoryAlias()};
		$related->offer_id = $this->id;
		return $related->save(false);
	}

	/**
	 *
	 */
	public function saveImages()
	{
		$data = ($this->tempImages) ? explode(',', $this->tempImages) : [];
		OfferImage::updateByOffer($this->id, $data);
	}

	/**
	 * @return array
	 */
	public function prepareImages()
	{
		$list = ($this->imagesList) ? explode(',', $this->imagesList) : [];
		if (empty($list)) {
			return $list;
		}
		foreach ($list as $i => $image) {
			$list[$i] = OfferImage::getThumbName($image);
		}
		return $list;
	}


	/**
	 * @param bool $valid
	 * @return bool
	 * @throws \Exception
	 */
	public function remove($valid = false)
	{
		if (!$valid && ($this->user_id != Yii::$app->user->id && !User::checkAccess('backend_offers'))) {
			return false;
		}
		/** @var OfferImage $i */
		foreach ($this->images as $i) {
			$i->delete();
		}
		$related = $this->getCategoryRelation();
		if ($related) {
			$related->delete();
		}
		$this->delete();
		return true;
	}

	/**
	 * @param $ids
	 * @return bool
	 */
	public static function removeByIds($ids)
	{
		$items = self::find()->andFilterWhere(['id' => $ids])->all();
		/** @var Offer $i */
		foreach ($items as $i) {
			$i->remove();
		}
		return true;
	}

	/**
	 * @return bool
	 */
	public function toArchive()
	{
		if ($this->user_id != Yii::$app->user->id && !User::checkAccess('backend_offers')) {
			return false;
		}
		$this->in_archive = true;
		return $this->save();
	}

	/**
	 * @param $ids
	 * @return bool
	 */
	public static function toArchiveByIds($ids)
	{
		$items = self::find()->andFilterWhere(['id' => $ids])->all();
		/** @var Offer $i */
		foreach ($items as $i) {
			$i->toArchive();
		}
		return true;
	}


	/**
	 * @return bool
	 */
	public function fromArchive()
	{
		if ($this->user_id != Yii::$app->user->id && !User::checkAccess('backend_offers')) {
			return false;
		}
		$this->in_archive = false;
		return $this->save();
	}

	/**
	 * @param $ids
	 * @return bool
	 */
	public static function fromArchiveByIds($ids)
	{
		$items = self::find()->andFilterWhere(['id' => $ids])->all();
		/** @var Offer $i */
		foreach ($items as $i) {
			$i->fromArchive();
		}
		return true;
	}

	/**
	 * @return bool
	 */
	public function approve()
	{
		if ($this->user_id != Yii::$app->user->id && !User::checkAccess('backend_offers')) {
			return false;
		}
		$this->is_approved = true;
		return $this->save();
	}

	/**
	 * @param $ids
	 * @return bool
	 */
	public static function approveByIds($ids)
	{
		$items = self::find()->andFilterWhere(['id' => $ids])->all();
		/** @var Offer $i */
		foreach ($items as $i) {
			$i->approve();
		}
		return true;
	}

	/**
	 * @return bool
	 */
	public function cancelApprove()
	{
		if ($this->user_id != Yii::$app->user->id && !User::checkAccess('backend_offers')) {
			return false;
		}
		$this->is_approved = false;
		return $this->save();
	}

	/**
	 * @param $ids
	 * @return bool
	 */
	public static function cancelApproveByIds($ids)
	{
		$items = self::find()->andFilterWhere(['id' => $ids])->all();
		/** @var Offer $i */
		foreach ($items as $i) {
			$i->cancelApprove();
		}
		return true;
	}

	/**
	 *
	 */
	public function handleView()
	{
		$this->views_total++;
		if (Helper::date($this->views_today_date, 'd.m.Y') != date('d.m.Y')) {
			$this->views_today_date = date('Y-m-d H:i:s');
			$this->views_today = 0;
		}
		$this->views_today++;
		$this->save();
	}

	/**
	 * @param $data
	 * @return bool
	 */
	public function updateData($data)
	{
		$this->attributes = $data;
		if (!$this->save()) {
			return false;
		}
		OfferImage::updateRemoteByOffer($this->id, $data['images']);
		return true;
	}

	/**
	 * @param $params
	 * @param null $userId
	 * @param bool $onlyManual
	 * @return ActiveDataProvider
	 */
	public function search($params, $userId = null, $onlyManual = false)
	{
		$query = self::find();
		$this->load($params);

		if (!isset($_GET['sort'])) {
			$query->orderBy(['date_updated' => SORT_DESC, 'date_created' => SORT_DESC]);
		}
		$query->joinWith(['location', 'flatData', 'user', 'user.agency']);
		$query
			->andFilterWhere(['category' => $this->category])
			->andFilterWhere(['type' => $this->type])
			->andFilterWhere(['user_id' => $this->user_id]);

		if ($userId) {
			$query->andWhere(['user_id' => $userId]);
		}

		if (in_array($this->in_archive, [1, 2])) {
			$query->andWhere('in_archive ' . ($this->in_archive == 1 ? 'is true' : 'is not true'));
		}

		if ($this->is_approved && in_array($this->is_approved, [1, 2])) {
			$query->andWhere('offer.is_approved ' . ($this->is_approved == 1 ? 'is true' : 'is not true'));
		}

		if (in_array($this->internal_id, [1, 2])) {
			$query->andWhere('internal_id ' . ($this->internal_id == 1 ? 'is null' : 'is not null'));
		}

		if ($onlyManual) {
			$query->andWhere('internal_id is NULL');
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$dataProvider->pagination->pageSize = 10;
		return $dataProvider;
	}

	/**
	 * @param $userId
	 * @return int
	 */
	public static function startSync($userId)
	{
		return self::updateAll(['sync' => false], ['user_id' => $userId]);
	}

	/**
	 * @param $userId
	 */
	public static function stopSync($userId)
	{
		/** @var Offer[] $offers */
		$offers = self::find()
			->where(['user_id' => $userId])
			->andWhere('internal_id is not null AND sync is not true')
			->all();
		foreach ($offers as $o) {
			$o->remove(true);
		}
	}
}
