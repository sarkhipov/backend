<?php

namespace common\components\meta;

use common\components\Helper;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 *
 */
class OfferMeta extends Component
{

	const TYPE_SALE = 1;
	const TYPE_RENT = 2;

	const CATEGORY_FLAT = 1;
	const CATEGORY_ROOM = 2;
	const CATEGORY_HOUSE = 3;
	const CATEGORY_LOT = 4;
	const CATEGORY_OFFICE = 5;

	const TIME_TO_METRO_5 = 5;
	const TIME_TO_METRO_10 = 10;
	const TIME_TO_METRO_15 = 15;

	const METRO_TRANSPORT_ON_FOOT = 1;
	const METRO_TRANSPORT_ON_TRANSPORT = 2;

	const CURRENCY_RUR = 'RUR';
	const CURRENCY_USD = 'USD';
	const CURRENCY_EUR = 'EUR';

	const PRICE_TYPE_PER_OFFER = 1;
	const PRICE_TYPE_PER_METER = 2;
	const PRICE_TYPE_PER_WEAVING = 3;
	const PRICE_TYPE_PER_HECTARE= 4;

	const PRICE_PERIOD_MONTH = 1;
	const PRICE_PERIOD_DAY = 2;

	const SELL_STATUS_FREE = 1;
	const SELL_STATUS_ALT = 2;

	/**
	 * @return array
	 */
	public static function getTypes()
	{
		return [
			self::TYPE_SALE => 'Купить',
			self::TYPE_RENT => 'Снять'
		];
	}

	/**
	 * @return array
	 */
	public static function getTypesAlt()
	{
		return [
			self::TYPE_SALE => 'Продать',
			self::TYPE_RENT => 'Сдать'
		];
	}

	/**
	 * @return array
	 */
	public static function getTypesAlt2()
	{
		return [
			self::TYPE_SALE => 'Продажа',
			self::TYPE_RENT => 'Аренда'
		];
	}

	/**
	 * @param $type
	 * @return null
	 */
	public static function getTypeName($type)
	{
		return Helper::getFromMap(self::getTypes(), $type);
	}

	/**
	 * @param $type
	 * @return null
	 */
	public static function getTypeNameAlt($type)
	{
		return Helper::getFromMap(self::getTypesAlt2(), $type);
	}

	/**
	 * @param $name
	 * @return null
	 */
	public static function getTypeByName($name)
	{
		return self::findInReversedArray(self::getTypesAlt2(), $name, self::TYPE_SALE);
	}

	/**
	 * @return array
	 */
	public static function getCategories()
	{
		return [
			self::CATEGORY_FLAT => 'Квартира',
			self::CATEGORY_ROOM => 'Комната',
			self::CATEGORY_HOUSE => 'Дом',
			self::CATEGORY_LOT => 'Участок',
			self::CATEGORY_OFFICE => 'Помещение',
		];
	}

	/**
	 * @return array
	 */
	public static function getCategoriesAlt()
	{
		return [
			'комната' => self::CATEGORY_ROOM, 'room' => self::CATEGORY_ROOM,
			'квартира' => self::CATEGORY_FLAT, 'flat' => self::CATEGORY_FLAT,
			'дом' => self::CATEGORY_HOUSE, 'house' => self::CATEGORY_HOUSE,
			'cottage' => self::CATEGORY_HOUSE, 'townhouse' => self::CATEGORY_HOUSE,
			'таунхаус' => self::CATEGORY_HOUSE, 'часть дома' => self::CATEGORY_HOUSE,
			'house with lot' => self::CATEGORY_HOUSE, 'дом с участком' => self::CATEGORY_HOUSE,
			'дача' => self::CATEGORY_HOUSE,
			'участок' => self::CATEGORY_LOT, 'lot' => self::CATEGORY_LOT,
			'земельный участок' => self::CATEGORY_LOT,
			'помещение' => self::CATEGORY_OFFICE, 'офис' => self::CATEGORY_OFFICE
		];
	}

	/**
	 * @param $category
	 *
	 * @return null
	 */
	public static function getCategoryName($category)
	{
		return Helper::getFromMap(self::getCategories(), $category);
	}

	/**
	 * @param $name
	 * @return null
	 */
	public static function getCategoryByName($name)
	{
		return Helper::getFromMap(self::getCategoriesAlt(), mb_strtolower($name ,'utf-8'), self::CATEGORY_FLAT);
	}

	/**
	 * @param $category
	 * @return null
	 */
	public static function getCategoryAlias($category)
	{
		$list = [
			self::CATEGORY_FLAT => 'flat',
			self::CATEGORY_ROOM => 'flat',
			self::CATEGORY_HOUSE => 'house',
			self::CATEGORY_LOT => 'lot',
			self::CATEGORY_OFFICE => 'office'
		];
		return Helper::getFromMap($list, $category);
	}


	/**
	 * @return array
	 */
	public static function getTimesToMetro()
	{
		return [
			self::TIME_TO_METRO_5 => 5,
			self::TIME_TO_METRO_10 => 10,
			self::TIME_TO_METRO_15 => 15,
		];
	}

	/**
	 * @param $timeToMetro
	 * @return null
	 */
	public static function getTimeToMetroName($timeToMetro)
	{
		return Helper::getFromMap(self::getTimesToMetro(), $timeToMetro);
	}

	/**
	 * @return array
	 */
	public static function getMetroTransports()
	{
		return [
			self::METRO_TRANSPORT_ON_FOOT => 'icon8.png',
			self::METRO_TRANSPORT_ON_TRANSPORT => 'icon9.png',
		];
	}

	/**
	 * @param $metroTransport
	 * @return null
	 */
	public static function getMetroTransportName($metroTransport)
	{
		return Helper::getFromMap(self::getMetroTransports(), $metroTransport);
	}

	/**
	 * @return array
	 */
	public static function getMetroTransportsAlt()
	{
		return [
			self::METRO_TRANSPORT_ON_FOOT => 'пешком',
			self::METRO_TRANSPORT_ON_TRANSPORT => 'транспортом',
		];
	}

	/**
	 * @param $metroTransport
	 * @return null
	 */
	public static function getMetroTransportNameAlt($metroTransport)
	{
		return Helper::getFromMap(self::getMetroTransportsAlt(), $metroTransport);
	}

	/**
	 * @return array
	 */
	public static function getCurrencies()
	{
		return [
			self::CURRENCY_RUR => 'руб.',
			self::CURRENCY_USD => '$',
			self::CURRENCY_EUR => '€',
		];
	}

	/**
	 * @param $currency
	 * @return null
	 */
	public static function getCurrencyName($currency)
	{
		return Helper::getFromMap(self::getCurrencies(), $currency);
	}

	/**
	 * @return array
	 */
	public static function getPriceTypes()
	{
		return [
			self::PRICE_TYPE_PER_OFFER => 'за всё',
			self::PRICE_TYPE_PER_METER => 'за м²',
		];
	}

	/**
	 * @return array
	 */
	public static function getPriceTypesAlt()
	{
		return [
			'кв. м' => self::PRICE_TYPE_PER_METER, 'sq.m' => self::PRICE_TYPE_PER_METER,
			'сотка' => self::PRICE_TYPE_PER_WEAVING,
			'гектар' => self::PRICE_TYPE_PER_HECTARE, 'hectare' => self::PRICE_TYPE_PER_HECTARE
		];
	}

	/**
	 * @param $priceType
	 * @return null
	 */
	public static function getPriceTypeName($priceType)
	{
		return Helper::getFromMap(self::getPriceTypes(), $priceType);
	}

	/**
	 * @return array
	 */
	public static function getPricePeriods()
	{
		return [
			self::PRICE_PERIOD_MONTH => 'в месяц',
			self::PRICE_PERIOD_DAY => 'в сутки',
		];
	}

	/**
	 * @return array
	 */
	public static function getPricePeriodsAlt()
	{
		return [
			'день' => self::PRICE_PERIOD_DAY, 'day' => self::PRICE_PERIOD_DAY,
			'месяц' => self::PRICE_PERIOD_MONTH, 'month' => self::PRICE_PERIOD_MONTH,
		];
	}

	/**
	 * @param $priceType
	 * @return null
	 */
	public static function getPricePeriodName($priceType)
	{
		return Helper::getFromMap(self::getPricePeriods(), $priceType);
	}

	/**
	 * @return array
	 */
	public static function getSellStatus()
	{
		return [
			self::SELL_STATUS_FREE => 'Свободная',
			self::SELL_STATUS_ALT => 'Альтернативная',
		];
	}

	/**
	 * @param $type
	 * @return null
	 */
	public static function getSellStatusName($type)
	{
		return Helper::getFromMap(self::getSellStatus(), $type);
	}

	/**
	 * @param $data
	 * @param $value
	 * @param null $default
	 * @return null
	 */
	public static function findInReversedArray($data, $value, $default = null)
	{
		array_walk($data, function(&$value) {
			$value = Helper::toLower($value);
		});
		$data = array_flip($data);
		return Helper::getFromMap($data, Helper::toLower($value), $default);
	}

	/**
	 * @param $value
	 * @return bool
	 */
	public static function convertBoolValue($value)
	{
		$trueValues = ['да', 'true', 'yes', 1, '1', '+', true];
		return in_array($value, $trueValues);
	}
}
