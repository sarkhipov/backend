<?php
namespace components\landing\rating;

/**
 * Class Factory
 * @package components\landing\rating
 */
class Factory
{
    protected static $_cache = array();

	/**
	 * @param $type
	 * @return bool
	 */
	protected static function _isTypeInCache($type)
	{
		return array_key_exists($type, self::$_cache);
	}

	/**
	 * @param IRating $obj
	 */
	protected static function _putInCache(IRating $obj)
	{
		self::$_cache[$obj::getId()] = $obj;
	}

    /**
     * @param $type
     * @return IRating
     */
    public static function make($type)
    {
        /** @var IRating $obj */
        if (self::_isTypeInCache($type)) {
            return self::$_cache[$type];
        }
        $obj = null;
        switch ($type) {
            case Type::ECpm:
                $obj = new ECpmRating();
                break;
        }
        if (!self::_isTypeInCache($obj::getId())) {
            self::_putInCache($obj);
        }
        return $obj;
    }

} 