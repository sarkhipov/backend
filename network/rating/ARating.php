<?php

namespace components\landing\rating;

/**
 * Class ARating
 * @package components\landing\rating
 */
abstract class ARating implements IRating
{
	const MY_TYPE = Type::None;
	const K_INCOME = "income";
	const K_HITS_CNT = "hits";

	/**
	 * @param $params
	 * @param $propertyName
	 * @param null $propertyKey
	 * @param bool $throwException
	 * @return null
	 * @throws \Exception
	 */
	public function getProperty($params, $propertyName, $propertyKey = null, $throwException = true)
	{
		$propertyKey = is_null($propertyKey) ? $propertyName : $propertyKey;
		$objPropertyVal = \CustomMap::get($params, $propertyKey, null);
		if (is_null($objPropertyVal)) {
			if ($throwException) {
				throw new \Exception("Object property can not be null");
			}
			else {
				return null;
			}
		}
		return $objPropertyVal;
	}

	/**
	 * @param array $data
	 * @return array
	 */
	public function getBatchRating(array $data)
	{
		$calculated = array();
		foreach ($data as $entity => $calculationDetails) {
			$calculated[$entity] = $this->getRating($calculationDetails);
		}
		return $calculated;
	}


	/**
	 * @return string
	 */
	public static function getId()
	{
		return static::MY_TYPE;
	}

} 