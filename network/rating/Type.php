<?php
namespace components\landing\rating;

/**
 * Class Type
 * @package components\landing\rating
 */
class Type
{
    const None = "NONE";
    const ECpm = "ECpm";
    const OptimizedECpm = "OptimizedECpm";
}