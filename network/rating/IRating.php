<?php
namespace components\landing\rating;

interface IRating
{
	/**
	 * @param $params
	 * @return mixed
	 */
    public function getRating($params);

	/**
	 * @param array $data
	 * @return mixed
	 */
    public function getBatchRating(array $data);

	/**
	 * @return mixed
	 */
    public static function getId();
} 