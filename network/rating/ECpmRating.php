<?php
namespace components\landing\rating;

/**
 * Class ECpmRating
 * @package components\landing\rating
 */
class ECpmRating extends ARating implements IRating
{
	const MY_TYPE = Type::ECpm;

	/**
	 * @return bool
	 */
	protected function _isPropertiesContainsErrors()
	{
		$isHasErrors = false;
		foreach (func_get_args() as $val) {
			if (!is_numeric($val)) {
				$isHasErrors = true;
				break;
			}
			if ($val < 0) {
				$isHasErrors = true;
				break;
			}
		}
		return $isHasErrors;
	}

	/**
	 * @param array $params
	 * @return float|int
	 */
	public function getRating($params)
	{
		$income = $this->getProperty($params, "_income", self::K_INCOME);
		$hits = $this->getProperty($params, "_hits", self::K_HITS_CNT);

		if ($this->_isPropertiesContainsErrors($income, $hits)) {
			return 0;
		}
		$thousands = $hits / 1000;
		if ($thousands == 0) {
			return 0;
		}
		return $income / $thousands;
	}
} 