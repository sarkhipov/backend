<?php

/**
 * Class StatusModel
 */
class StatusModel extends Model
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    use StatusModel_trait;
}