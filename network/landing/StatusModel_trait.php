<?php

/**
 * Class StatusModel_trait
 */
trait StatusModel_trait
{
    /**
     * @return array
     */
    public function getStatusList()
    {
        return array(
            self::STATUS_ACTIVE => i18n::t("Active"),
            self::STATUS_INACTIVE => i18n::t("Inactive"),
        );
    }

    /**
     * @return mixed
     */
    public function getStatusName()
    {
        $list = $this->getStatusList();
        return $list[$this->status];
    }

    /**
     * @return array|mixed|null|static
     */
    public function getActive()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("status = :s");
        $criteria->params[':s'] = self::STATUS_ACTIVE;
        return $this->findAll($criteria);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status === StatusModel::STATUS_ACTIVE;
    }
}