<?php

/**
 * This is the model class for table "landing".
 *
 * The followings are the available columns in table 'landing':
 * @property integer $id
 * @property integer $landing_config_id
 * @property integer $name
 * @property integer $description
 * @property string $status
 * @property string $params
 * @property string $ticket_id
 * @property string $config_override
 *
 * The followings are the available model relations:
 * @property LandingConfig $config
 * @property LandingGroupLanding[] $landingGroups
 */
class Landing extends StatusModel
{

    public static $ratingMap = array();

    /**
     * @var BaseLanding
     */
    protected $_classObject = null;

	/**
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->params = json_encode($this->params);
		return parent::beforeSave();
	}

	/**
	 *
	 */
	protected function afterFind()
	{
		$this->params = ($this->params && $this->params !== 'null')
			? json_decode($this->params, true)
			: array();
		foreach ($this->params as $k => $v) {
			if (!$v) {
				continue;
			}
			$this->paramsSummary[] = "$k: $v";
		}
		parent::afterFind();
	}

	/**
	 *
	 */
	protected function initGroups()
	{
		foreach ($this->landingGroups as $item) {
			$this->_groups[] = $item->id;
			$this->_groupNames[] = $item->name;
		}
	}

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'landing';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'config' => array(self::BELONGS_TO, 'LandingConfig', 'landing_config_id'),
            'landingGroups' => array(
                self::MANY_MANY,
                'LandingGroup',
                'landing_group_landing(landing_id, landing_group_id)'
            ),
            'ldapUsers' => array(
                self::MANY_MANY,
                'LdapUser',
                'landing_ldap_user(landing_id, ldap_user_id)'
            ),
            'campaigns' => array(self::MANY_MANY, 'Campaign', 'campaign_landing(campaign_id, landing_id)'),
        );
    }


	/**
	 * @param $attribute
	 * @return bool
	 */
	public function validateParams($attribute)
	{
		if (!$this->params || empty($this->params)) {
			return true;
		}
		$landing = Landing::model()->find(
			'landing_config_id = :id AND params = :params',
			array(':id' => $this->landing_config_id, ':params' => json_encode($this->params))
		);
		if ($landing && $landing->id !== $this->id) {
			$this->addError($attribute, i18n::t("Landing with such config and params already exist"));
		}
		return true;
	}


	/**
	 * @param $ratingParams
	 * @return mixed
	 */
    public function calculateRating($ratingParams)
    {
        $conversionType = CustomMap::get($ratingParams, 'conversion_type', null);
        if(!$conversionType) {
            $conversionType =  \components\landing\rating\Type::ECpm;
        }

        $rating = \components\landing\rating\Factory::make($conversionType);
        $ratingData = $rating->getBatchRating($ratingParams);

        return $ratingData;
    }

    /**
     * @param $id
     * @return Landing|null
     */
    public function getById($id)
    {
        $cr = new CDbCriteria();
        $cr->compare('t.id', $id);
        $cr->with = array('config');
        $cr->together = true;
        return Landing::model()->find($cr);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Landing the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

}
