# realty #

Фрагмент серверного кода портала недвижимости. 

Offer.php - класс модели yii2, сущность объявления о продаже или аренде недвижимости. Содержит методы для создания, обновления и удаления объявлений, набор методов для работы со связанными данными - объектами, изображениями, расширяющими моделями. Так же ряд методов подготавливают необходимую информацию для вывода на клиентской стороне.

OfferMeta.php - мета-класс с набором данных о параметрах объявления.

# network #

Фрагмент кода инфраструктуры рекламной сети.

## rating ##

Набор классов для подсчета рейтинга landing-страниц. Объекты создаются через фабрику Factory, в зависимости от типа подсчета. Объекты конечных классов (в примере - ECpmRating) наследуются от абстрактного класса ARating, который, в свою очередь, реализует интерфейс IRating. 

## landing ##

Landing.php - класс модели yii. Наследуется от базового класса StatusModel, реализующего функциональность смены своего статуса через использования трейта StatusModel_trait.

В рамках объекта landing'а производится подсчет рейтинга на основе объекта фабрики Rating.